# KrapLoot
## Deals with all that Krap!
I present to you: KrapLoot, your go-to add-on for getting rid of all those stupid-ass loot windows during a ZG Run.

It supports Bijou, Coins, Bloodvine, Demonic/Dark Rune and Abyssal Crest/Signet. 

It's incredibly lightweight as it only affects those items.

### Installation
Just drop the "KrapLoot" folder into:
`C:\Program Files (x86)\World of Warcraft\_classic_\Interface\AddOns\`
(Or wherever you installed it)

### Usage
You can toggle the options using the GUI under `Interface Options > Add-ons > KrapLoot`

If you prefer to use slash-commands, don't let me stop you:

You can use `/krap`, `/kl` or `/kraploot` interchangeably.

e.g. To turn on rollhacks, type `/kl rollhacks 1`

Show current configuration
```
/krap
```

Toggle auto-greed Coin/Bijou
```
/krap bijou [0|1]
/krap coin [0|1]
```

Toggle auto-pass on <item>
```
/krap bloodvine [0|1]
/krap demonicrune [0|1]
/krap darkrune [0|1]
/krap abyssalcrest [0|1]
/krap abyssalsignet [0|1]
```