-- Config Defaults
if not KrapLootDB then
    KrapLootDB = { 
        enableBijou = true, 
        enableCoin = true, 
        enableBloodvine = true, 
        enableDemonicRune = true, 
        enableDarkRune = true, 
        enableAbyssalCrest = true, 
        enableAbyssalSignet = true }
end

-- Commands
local function SlashCmd(key, value, ...)
    if key and key ~= "" then
        -- Bijou
        if key == "bijou" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableBijou = enable
            Krap_ConfigFrame.chkBtnBijou:SetChecked(KrapLootDB.enableBijou);
            Krap_Print("'Greed on Bijou' set: "..( enable and "true" or "false" ))
        -- Coin
        elseif key == "coin" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableCoin = enable
            Krap_ConfigFrame.chkBtnEnableCoin:SetChecked(KrapLootDB.enableCoin);
            Krap_Print("'Greed on Coin' set: "..( enable and "true" or "false" ))
        -- Bloodvine
        elseif key == "bloodvine" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableBloodvine = enable
            Krap_ConfigFrame.chkBtnEnableBloodvine:SetChecked(KrapLootDB.enableBloodvine);
            Krap_Print("'Pass on Bloodvine' set: "..( enable and "true" or "false" ))
        -- Demonic Rune
        elseif key == "demonicrune" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableDemonicRune = enable
            Krap_ConfigFrame.chkBtnEnableDemonicRune:SetChecked(KrapLootDB.enableDemonicRune);
            Krap_Print("'Pass on Demonic Rune' set: "..( enable and "true" or "false" ))
        -- Dark Rune
        elseif key == "darkrune" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableDarkRune = enable
            Krap_ConfigFrame.chkBtnEnableDarkRune:SetChecked(KrapLootDB.enableDarkRune);
            Krap_Print("'Pass on Dark Rune' set: "..( enable and "true" or "false" ))
        -- Abyssal Crest
        elseif key == "abyssalcrest" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableAbyssalCrest = enable
            Krap_ConfigFrame.chkBtnEnableAbyssalCrest:SetChecked(KrapLootDB.enableAbyssalCrest);
            Krap_Print("'¨Pass on Abyssal Crest' set: "..( enable and "true" or "false" ))
        -- Abyssal Signet
        elseif key == "abyssalsignet" and tonumber(value) then
            local enable = tonumber(value) == 1 and true or false
            KrapLootDB.enableAbyssalSignet = enable
            Krap_ConfigFrame.chkBtnEnableAbyssalSignet:SetChecked(KrapLootDB.enableAbyssalSignet);
            Krap_Print("'Pass on Abyssal Signet' set: "..( enable and "true" or "false" ))
        -- Help
        elseif key == "help" then
            Krap_Print("You can use '/kraploot', '/krap' or '/kl'")                
            Krap_Print("Just '/krap' shows the current config.")
            Krap_Print("'0' turns it off. '1' Turns it on. It's not rocket science.")
            Krap_Print("Automatically greed on Bijou/Coin")
            Krap_Print("/krap bijou 0/1")
            Krap_Print("/krap coin 0/1")
            Krap_Print("Automatically pass on <item>")
            Krap_Print("/krap bloodvine 0/1")
            Krap_Print("/krap demonicrune 0/1")
            Krap_Print("/krap darkrune 0/1")
            Krap_Print("/krap abyssalcrest 0/1")     
            Krap_Print("/krap abyssalsignet 0/1") 
        -- Easter Egg (why are you going through my code?!)      
        elseif key == "krapuul" then
            r = math.random(0,4);
            if r == 0 then
                s ="Krapuul is the best warrior ever."
                DoEmote("AMAZE", "Krapuul")
            elseif r == 1 then
                s ="I should give Krapuul all my gold for his awesomeness."
                DoEmote("CHEER", "Krapuul")
            elseif r == 2 then
                s ="AWWA!"
                DoEmote("ROAR")
            elseif r == 3 then
                s ="Biggus-who?"
                DoEmote("RASP")
            elseif r == 4 then
                s ="LOOT THE DOG!"
                DoEmote("ANGRY")
            else -- (Default)
                s ="ERROR!"
            end
            SendChatMessage(s , "SAY", nil, index); 
        end
    else
        -- Current Config
        Krap_Print("Current Config")
        Krap_Print("Greed Bijou: "..( KrapLootDB.enableBijou and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Greed Coin: "..( KrapLootDB.enableCoin and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Pass Bloodvine: "..( KrapLootDB.enableBloodvine and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Pass Demonic Rune: "..( KrapLootDB.enableDemonicRune and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Pass Dark Rune: "..( KrapLootDB.enableDarkRune and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Pass Abyssal Crest: "..( KrapLootDB.enableAbyssalCrest and "|cff00ff00true" or "|cffff0000false" ))
        Krap_Print("Pass Abyssal Signet: "..( KrapLootDB.enableAbyssalSignet and "|cff00ff00true" or "|cffff0000false" ))
    end
end

SLASH_KRAPLOOT1 = "/kraploot";
SLASH_KRAPLOOT2 = "/krap";
SLASH_KRAPLOOT3 = "/kl";
SlashCmdList["KRAPLOOT"] = function(msg)
    msg = string.lower(msg)
    msg = { string.split(" ", msg) }
    if #msg >= 1 then
        local exec = table.remove(msg, 1)
        SlashCmd(exec, unpack(msg))
    end
end

-- Print handler
function Krap_Print(msg)
	print("|cff00ff00Krap|cffffcc00Loot|r: "..(msg or ""))
end


-- Interface Options
function Krap_UI()
    Krap_ConfigFrame = CreateFrame("Frame", "Krap_InterfaceOptionsPanel", UIParent);
    Krap_ConfigFrame.name = "KrapLoot";
    InterfaceOptions_AddCategory(Krap_ConfigFrame);

    -- Title, Content and font
    Krap_ConfigFrame.title = CreateFrame("Frame", Krap_ConfigFrame.name, Krap_ConfigFrame);
    Krap_ConfigFrame.title:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 10, -30);
    Krap_ConfigFrame.title:SetWidth(300);
    Krap_ConfigFrame.titleString = Krap_ConfigFrame.title:CreateFontString(nil, "OVERLAY", "GameFontNormalLarge");
    Krap_ConfigFrame.titleString:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 10, -15);
    Krap_ConfigFrame.titleString:SetText('KrapLoot');
    Krap_ConfigFrame.titleString:SetFont("Fonts\\FRIZQT__.tff", 20, "OUTLINE");
	Krap_ConfigFrame.titleString:SetJustifyH("LEFT")
    Krap_ConfigFrame.titleString:SetJustifyV("TOP")

	Krap_ConfigFrame.content = CreateFrame("Frame", "Krap_OptionsContent", frame)
	Krap_ConfigFrame.content:SetPoint("TOPLEFT", 10, -10)
    Krap_ConfigFrame.content:SetPoint("BOTTOMRIGHT", -10, 10)

    Krap_ConfigFrame.content = content

    -- Slash Commands
	Krap_ConfigFrame.Label = Krap_ConfigFrame:CreateFontString(nil, 'ARTWORK', 'GameFontHighlightSmallOutline')
	Krap_ConfigFrame.Label:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 20, -45)
	Krap_ConfigFrame.Label:SetJustifyH("LEFT")
	Krap_ConfigFrame.Label:SetJustifyV("TOP")
    Krap_ConfigFrame.Label:SetText("You can also configure using '/krap', type '/krap help' for all commands.")

    -- Zul'Gurub
	Krap_ConfigFrame.Label = Krap_ConfigFrame:CreateFontString(nil, 'ARTWORK', 'GameFontHighlightSmallOutline')
	Krap_ConfigFrame.Label:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 20, -80)
	Krap_ConfigFrame.Label:SetJustifyH("LEFT")
	Krap_ConfigFrame.Label:SetJustifyV("TOP")
    Krap_ConfigFrame.Label:SetText("Zul'Gurub")
    
    -- Checkboxes
    -- Checkbox Bijou
    Krap_ConfigFrame.chkBtnBijou = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnBijou:SetPoint("TOPLEFT", 20, -100);
    Krap_ConfigFrame.chkBtnBijou.text:SetText("Greed Bijou");
    Krap_ConfigFrame.chkBtnBijou:SetChecked(KrapLootDB.enableBijou);
    Krap_ConfigFrame.chkBtnBijou:SetScript("OnClick", 
    function()
        KrapLootDB.enableBijou = not KrapLootDB.enableBijou;
    end);

    -- Checkbox Coin
    Krap_ConfigFrame.chkBtnEnableCoin = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableCoin:SetPoint("TOPLEFT", 20, -130);
    Krap_ConfigFrame.chkBtnEnableCoin.text:SetText("Greed Coin");
    Krap_ConfigFrame.chkBtnEnableCoin:SetChecked(KrapLootDB.enableCoin);
    Krap_ConfigFrame.chkBtnEnableCoin:SetScript("OnClick", 
    function()
        KrapLootDB.enableCoin = not KrapLootDB.enableCoin;
    end);

    -- Checkbox Bloodvine
    Krap_ConfigFrame.chkBtnEnableBloodvine = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableBloodvine:SetPoint("TOPLEFT", 20, -160);
    Krap_ConfigFrame.chkBtnEnableBloodvine.text:SetText("Pass Bloodvine");
    Krap_ConfigFrame.chkBtnEnableBloodvine:SetChecked(KrapLootDB.enableBloodvine);
    Krap_ConfigFrame.chkBtnEnableBloodvine:SetScript("OnClick", 
    function()
        KrapLootDB.enableBloodvine = not KrapLootDB.enableBloodvine;
    end);

    -- Label Scholomance
    Krap_ConfigFrame.Label = Krap_ConfigFrame:CreateFontString(nil, 'ARTWORK', 'GameFontHighlightSmallOutline')
	Krap_ConfigFrame.Label:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 20, -200)
	Krap_ConfigFrame.Label:SetJustifyH("LEFT")
	Krap_ConfigFrame.Label:SetJustifyV("TOP")
    Krap_ConfigFrame.Label:SetText("Scholomance")
    
    -- Checkbox Dark Rune
    Krap_ConfigFrame.chkBtnEnableDarkRune = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableDarkRune:SetPoint("TOPLEFT", 20, -220);
    Krap_ConfigFrame.chkBtnEnableDarkRune.text:SetText("Pass Dark Rune");
    Krap_ConfigFrame.chkBtnEnableDarkRune:SetChecked(KrapLootDB.enableDarkRune);
    Krap_ConfigFrame.chkBtnEnableDarkRune:SetScript("OnClick", 
    function()
        KrapLootDB.enableDarkRune = not KrapLootDB.enableDarkRune;
    end);

    -- Label Open World    
	Krap_ConfigFrame.Label = Krap_ConfigFrame:CreateFontString(nil, 'ARTWORK', 'GameFontHighlightSmallOutline')
	Krap_ConfigFrame.Label:SetPoint("TOPLEFT", Krap_ConfigFrame, "TOPLEFT", 20, -260)
	Krap_ConfigFrame.Label:SetJustifyH("LEFT")
	Krap_ConfigFrame.Label:SetJustifyV("TOP")
    Krap_ConfigFrame.Label:SetText("Open World")

    -- Checkbox Demonic Rune
    Krap_ConfigFrame.chkBtnEnableDemonicRune = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableDemonicRune:SetPoint("TOPLEFT", 20, -280);
    Krap_ConfigFrame.chkBtnEnableDemonicRune.text:SetText("Pass Demonic Rune");
    Krap_ConfigFrame.chkBtnEnableDemonicRune:SetChecked(KrapLootDB.enableDemonicRune);
    Krap_ConfigFrame.chkBtnEnableDemonicRune:SetScript("OnClick", 
    function()
        KrapLootDB.enableDemonicRune = not KrapLootDB.enableDemonicRune;
    end);

    -- Checkbox Abyssal Crest
    Krap_ConfigFrame.chkBtnEnableAbyssalCrest = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableAbyssalCrest:SetPoint("TOPLEFT", 20, -310);
    Krap_ConfigFrame.chkBtnEnableAbyssalCrest.text:SetText("Pass Abyssal Crest");
    Krap_ConfigFrame.chkBtnEnableAbyssalCrest:SetChecked(KrapLootDB.enableAbyssalCrest);
    Krap_ConfigFrame.chkBtnEnableAbyssalCrest:SetScript("OnClick", 
    function()
        KrapLootDB.enableAbyssalCrest = not KrapLootDB.enableAbyssalCrest;
    end);

    -- Checkbox Abyssal Signet
    Krap_ConfigFrame.chkBtnEnableAbyssalSignet = CreateFrame("CheckButton", "default", Krap_ConfigFrame, "UICheckButtonTemplate");
    Krap_ConfigFrame.chkBtnEnableAbyssalSignet:SetPoint("TOPLEFT", 20, -340);
    Krap_ConfigFrame.chkBtnEnableAbyssalSignet.text:SetText("Pass Abyssal Signet");
    Krap_ConfigFrame.chkBtnEnableAbyssalSignet:SetChecked(KrapLootDB.enableAbyssalSignet);
    Krap_ConfigFrame.chkBtnEnableAbyssalSignet:SetScript("OnClick", 
    function()
        KrapLootDB.enableAbyssalSignet = not KrapLootDB.enableAbyssalSignet;
    end);
end

-- Serializing the DB
local krapLoader = CreateFrame("Frame");
krapLoader:RegisterEvent("ADDON_LOADED");
krapLoader:RegisterEvent("START_LOOT_ROLL");
krapLoader:RegisterEvent("CONFIRM_LOOT_ROLL");

-- Initialization
function krapLoader:OnEvent(event, arg1)
    if (event == "ADDON_LOADED" and arg1 == "KrapLoot") then
        Krap_UI();
    end
    if (event =="START_LOOT_ROLL") then
        rollID = arg1;
        local _, name, _, _, _, canNeed, canGreed, canDisenchant, reasonNeed, reasonGreed, reasonDisenchant = GetLootRollItemInfo(rollID);

        -- Bijou
        if KrapLootDB.enableBijou == true then
            if name == "Blue Hakkari Bijou" then
                RollOnLoot(rollID,2);
            elseif name == "Bronze Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Gold Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Green Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Orange Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Purple Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Red Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Silver Hakkari Bijou" then            
                RollOnLoot(rollID,2);
            elseif name == "Yellow Hakkari Bijou" then  
                RollOnLoot(rollID,2);
            end
        end
        -- Coin
        if KrapLootDB.enableCoin == true then
            if name == "Bloodscalp Coin" then            
                RollOnLoot(rollID,2);
            elseif name == "Gurubashi Coin" then            
                RollOnLoot(rollID,2);
            elseif name == "Hakkari Coin" then            
                RollOnLoot(rollID,2);
            
            elseif name == "Razzashi Coin" then            
                RollOnLoot(rollID,2);
            
            elseif name == "Sandfury Coin" then           
                RollOnLoot(rollID,2);
            
            elseif name == "Skullsplitter Coin" then            
                RollOnLoot(rollID,2);
            
            elseif name == "Vilebranch Coin" then            
                RollOnLoot(rollID,2);
            
            elseif name == "Witherbark Coin" then            
                RollOnLoot(rollID,2);
            
            elseif name == "Zulian Coin" then            
                RollOnLoot(rollID,2);
            end
        end            
        
        -- Bloodvine        
        if KrapLootDB.enableBloodvine == true then
            if name == "Bloodvine" then
                RollOnLoot(rollID,0);
            end
        end

        -- Dark Rune
        if KrapLootDB.enableDarkRune == true then        
            if name == "Dark Rune" then
                RollOnLoot(rollID,0);
            end
        end

        -- Demonic Rune
        if KrapLootDB.enableDemonicRune == true then
            if name == "Demonic Rune" then
                RollOnLoot(rollID,0);
            end
        end

        -- Abyssal Crest
        if KrapLootDB.enableAbyssalCrest == true then        
            if name == "Abyssal Crest" then
                RollOnLoot(rollID,0);
            end
        end

        -- Abyssal Signet
        if KrapLootDB.enableAbyssalSignet == true then        
            if name == "Abyssal Signet" then
                RollOnLoot(rollID,0);
            end
        end
    end
end

krapLoader:SetScript("OnEvent", krapLoader.OnEvent);